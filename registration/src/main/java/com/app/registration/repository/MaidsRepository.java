package com.app.registration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.registration.model.Maids;
//import com.app.registration.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface MaidsRepository extends JpaRepository <Maids, Integer> {

	//public Maids findByLocation(String location);
	
//	 @Query(value = "SELECT * FROM maids_list t where (t.Type = ?1 And t.Location = ?2)", 
//		        nativeQuery=true
	 
	@Query(value = "SELECT * from  maids_list m where m.location =:s1", 
	 nativeQuery=true
	)
	 public List<Maids> getByName(@Param("s1") String s1);
	
	@Override
	List<Maids> findAll();
	
	 //public List<Maids> findByTypeAndLocation(String Type, String Location);
	//public List<Maids> findByTypeAndLocation(String Type);
	
	//public Optional<Todo> findByTitleAndDescription(String title, String description);
	
}