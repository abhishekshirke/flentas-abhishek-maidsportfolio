package com.app.registration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.registration.model.Maids;
import com.app.registration.model.User;
import org.springframework.data.jpa.repository.Query;
public interface RegistrationRepository extends JpaRepository <User, Integer> {

	public User findByEmailId(String emailId);
	public User findByEmailIdAndPassword(String emailId, String password);
	//public Maids findByLocation(String location);
	
//	 @Query(value = "SELECT * FROM maids_list t where (t.Type = ?1 And t.Location = ?2)", 
//		        nativeQuery=true
	 
	
	 //public List<Maids> findByTypeAndLocation(String Type, String Location);
	//public List<Maids> findByTypeAndLocation(String Type);
	
	//public Optional<Todo> findByTitleAndDescription(String title, String description);
	
}