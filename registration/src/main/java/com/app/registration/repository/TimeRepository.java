package com.app.registration.repository;


import java.util.List;

import javax.transaction.Transactional;

//import com.app.registration.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.registration.model.Maids;
import com.app.registration.model.Time;
public interface TimeRepository extends JpaRepository <Time, Integer> {

	
//	 @Query(value = "SELECT * FROM maids_list t where (t.Type = ?1 And t.Location = ?2)", 
//		        nativeQuery=true
	 
	//@Query(value = "SELECT * from  time t where t.name =:s1}", 
	// nativeQuery=true
	//	)
	
	//public List<Maids> showMaidsByLC(@Param("s1") String s1);
	
	//List<Time> findByName();
	@Override
	List<Time> findAll();
	
	@Query(value = "SELECT * from  time t where t.name =:s1",
	nativeQuery=true)		
	public List<Time> getByName(@Param("s1") String s1);
	
	@Modifying
	@Transactional
	@Query(value = "Update Time t set t.bit = 0 where (t.time =:t1 && t.name = :n1)",
			nativeQuery=true)		
			public void getByTime(@Param("t1") String t1,@Param("n1") String n1);
	
}