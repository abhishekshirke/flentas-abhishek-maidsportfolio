package com.app.registration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.registration.model.Maids;
import com.app.registration.model.Time;
import com.app.registration.model.User;
import com.app.registration.repository.MaidsRepository;
import com.app.registration.repository.RegistrationRepository;
import java.util.List;


@Service
public class MaidsService {
	
	@Autowired
	private MaidsRepository repo;
	

	public List<Maids> showMaids() {
		//return (Maids) repo.findByTypeAndLocation();
		return repo.findAll();
		// repo.findAll();
	}
	
//	public List<Maids> showMaidsByLC1(String s1) {
//		
//		return (List<Maids>) repo.showMaidsByLC(s1);
//		
//	}

	public List<Maids> getByName(String location) {
		
		return repo.getByName(location);
	}
	
	
}
