package com.app.registration.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.registration.model.Maids;
import com.app.registration.model.User;
import com.app.registration.repository.RegistrationRepository;
import java.util.List;


@Service
public class RegistrationService {
	
	@Autowired
	private RegistrationRepository repo;
	
	public User saveUser(User user) {
		
		return repo.save(user);
		
	}
	public User fetchUserByEmailId(String email) {
		
		return repo.findByEmailId(email);
		
	}
		
	public User fetchUserByEmailIdAndPassword(String email, String password) {
		
		return repo.findByEmailIdAndPassword(email, password);
		
	}
	
	//public Maids showMaids(String type, String location) {
	
//	public List<Maids> showMaids(String type, String location) {
//		return (List<Maids>) repo.findByTypeAndLocation(type,location);
//	}
	
	//public List<Maids> showMaids() {
	//	return (List<Maids>) repo.findByTypeAndLocation();
	//	// repo.findAll();
	//}
	
	
}
