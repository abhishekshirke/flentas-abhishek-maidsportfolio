package com.app.registration.model;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name = "maids_list")
public class Maids {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "Name")
	private String Name;
	
	@Column(name = "Catagory")
	private String Catagory;
	
	@Column(name = "Location")
	private String Location;
	
	public Maids() {
		
	}
	
//	public Maids(String Name,String location, String Catagory) {
//		super();
//		this.Name = Name;
//		this.Catagory = Catagory;
//		this.Location = location;
//	}
	
	

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getCatagory() {
		return Catagory;
	}

	public void setCatagory(String catagory) {
		Catagory = catagory;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	
}
