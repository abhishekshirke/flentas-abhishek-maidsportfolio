package com.app.registration.controller;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.registration.model.Maids;
import com.app.registration.model.User;
import com.app.registration.service.RegistrationService;

@RestController
public class RegistraionController {
	
	@Autowired
	private RegistrationService service;
	
	@PostMapping("/registeruser")
	@CrossOrigin(origins = "http://localhost:4200")
	public User registerUser(@RequestBody User user) throws Exception {
		String tempEmailId = user.getEmailId();
//		System.out.println("Text");

		if(tempEmailId != null && !"".equals(tempEmailId)) {
			
			User userobject = service.fetchUserByEmailId(tempEmailId);
			if(userobject != null) {
				
				throw new Exception("user with"+tempEmailId+" is already exists");
			}
		
		} 
		User userObj = null;
		userObj = service.saveUser(user);
		return userObj;
	}
	
	@PostMapping("/login")
	@CrossOrigin(origins = "http://localhost:4200")
	public User loginUser(@RequestBody User user) throws Exception {
			
		String tempEmailId = user.getEmailId();
		String tempPassword = user.getPassword();
		
		User userobject = null;
		
		if( tempEmailId != null && tempPassword != null) {
			
			userobject = service.fetchUserByEmailIdAndPassword(tempEmailId, tempPassword);
				
		}
		
		//System.out.println(""+tempEmailId+tempPassword);
		
		if(userobject == null) {
			
			throw new Exception("Bad Credentials");
			
		}
		
		return userobject;
		
	}
	
//	@GetMapping("/show")
//	//@RequestMapping(value = "/show{/Type}", method = RequestMethod.GET)
//	@CrossOrigin(origins = "http://localhost:4200")
//	//public List<String> showMaids( @RequestParam("Type") String s1, @RequestParam("Location") String s2) throws Exception {
//	//public List<Maids> showMaids(@RequestBody Maids maid) throws Exception {	
//	public List<Maids> showMaids() throws Exception {	
//			
//		//String tempType = maid.getType();
//		//String tempLocation = maid.getLocation();
//		
//		//List<Maids> m = service.showMaids("Cooking","Pune");
//		
//		//System.out.println(tempType);
//		//System.out.println(tempLocation);
//		//System.out.println("show maids");
//		//return Maids service.showMaids(tempType, tempLocation);
//		//System.out.println(tempType);
//		//System.out.println(tempLocation);
//		
//		//return (List<Maids>)this.service.showMaids(tempType,tempLocation);
//		//return (List<Maids>)this.service.showMaids();
//		return this.service.showMaids();
	
}
	
	

