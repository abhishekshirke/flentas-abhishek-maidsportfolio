import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {Observable} from "rxjs/index";
import { ApiResponse } from '../model/api.response';
import { Users } from '../model/users.model';
import { environment } from 'src/environments/environment';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:9070/';

  

  // getEmployees() : Observable<ApiResponse> {
  //   return this.http.get<ApiResponse>(this.baseUrl);
  // }

  getMaidByLocation(location: String): Observable<any> {
     return this.http.get(this.baseUrl + location);
  }

  registeruser(user: Users): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, user);
  }

  // updateEmployee(id: number, employee: Employee): Observable<ApiResponse> {
  //   return this.http.put<ApiResponse>(this.baseUrl + employee.id, employee);
  // }

  // deleteEmployee(id: number): Observable<ApiResponse> {
  //   return this.http.delete<ApiResponse>(this.baseUrl + id);
  // }
}