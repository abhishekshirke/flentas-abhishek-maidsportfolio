import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MaidsListComponent } from './maids/maids-list/maids-list.component';
import { RegisteruserComponent } from './user/registeruser/registeruser.component';

const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'register', component: RegisteruserComponent },
  { path: 'maids', component: MaidsListComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
