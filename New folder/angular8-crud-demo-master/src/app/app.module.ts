import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersService } from './service/users.service';
import { HttpClientModule } from '@angular/common/http';
//import { EmployeeListComponent } from './employee/employee-list/employee-list.component';
//import { UpdateEmployeeComponent } from './employee/update-employee/update-employee.component';
import { DataTablesModule } from 'angular-datatables';
import { RegisteruserComponent } from './user/registeruser/registeruser.component';
import { MaidsListComponent } from './maids/maids-list/maids-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisteruserComponent,
    MaidsListComponent,
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    DataTablesModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
