import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaidsListComponent } from './maids-list.component';

describe('MaidsListComponent', () => {
  let component: MaidsListComponent;
  let fixture: ComponentFixture<MaidsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaidsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaidsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
