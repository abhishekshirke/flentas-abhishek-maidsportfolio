import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiResponse } from 'src/app/model/api.response';
import {UsersService} from 'src/app/service/users.service';

@Component({
  selector: 'app-maids-list',
  templateUrl: './maids-list.component.html',
  styleUrls: ['./maids-list.component.css']
})
export class MaidsListComponent implements OnInit {

  user:Observable<ApiResponse>
  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
    this.usersService.getMaidByLocation(String);
  }

}
