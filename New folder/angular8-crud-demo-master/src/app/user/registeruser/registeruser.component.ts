import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from 'src/app/model/users.model';
import {UsersService} from 'src/app/service/users.service'
@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.css']
})
export class RegisteruserComponent implements OnInit {
  user:Users = new Users();
  submitted=false;

  constructor(private usersService: UsersService, private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit() {

    this.submitted=true;
    this.usersService.registeruser(this.user).subscribe(

        data=> console.log(data), error=> console.log(error));
        this.user=new Users();
        this.router.navigate(['/user']);

  }

}
