import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

   user = new User();
   msg = '';
   userMail: String;
  
  constructor(private _service : RegistrationService, private _router : Router) { }

  ngOnInit() {
  }

  loginUser(m1: String) {
    this.userMail = m1;

    this._service.loginUserFromRemote(this.user).subscribe(

        data => { 
          console.log("responce recived");
          //this._router.navigate(['/loginsuccess'])
          console.log(this.userMail);
          this._service.setUserMail(this.userMail)
          this._router.navigate(['/selectlocation'])
        } ,
        
        error => { 
          
          console.log("exception occured");
          this.msg = "Bad Credentials, Please Enter Valid EmailId and Password";
        
       }
    )
  }

  gotoregistration() {
    
    this._router.navigate(['/registration'])
  }
}
