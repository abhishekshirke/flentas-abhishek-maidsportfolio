import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginsuccessComponent } from './loginsuccess/loginsuccess.component';
import { SelectlocationComponent } from './selectlocation/selectlocation.component';
import { ShowdetailsComponent } from './showdetails/showdetails.component';
import { BookslotComponent } from './bookslot/bookslot.component';
import { PopupComponent } from './popup/popup.component';
// import {PopupModule} from 'ng2-opd-popup';
//import { ShowmaidsComponent } from './showmaids/showmaids.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    LoginsuccessComponent,
    SelectlocationComponent,
    ShowdetailsComponent,
    BookslotComponent,
    PopupComponent,
    //ShowmaidsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
