import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginsuccessComponent } from './loginsuccess/loginsuccess.component';
import { RegistrationService } from './registration.service';
import { RegistrationComponent } from './registration/registration.component';
import { SelectlocationComponent } from './selectlocation/selectlocation.component';
import { ShowdetailsComponent } from './showdetails/showdetails.component';
import { BookslotComponent } from './bookslot/bookslot.component';
import { PopupComponent } from './popup/popup.component';


const routes: Routes = [

  {path:'',component:LoginComponent},
  //{path:'loginsuccess',component:LoginsuccessComponent},
  {path:'registration',component:RegistrationComponent},
  {path:'selectlocation',component:SelectlocationComponent},
  {path:'login',component:LoginComponent},
  {path:'showdetails', component:ShowdetailsComponent},
  {path: 'bookslot', component:BookslotComponent},
  {path: 'popup', component:PopupComponent},
];
@NgModule({
  
  // declarations: [],
  // imports: [
  //   CommonModule
  // ]

    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
