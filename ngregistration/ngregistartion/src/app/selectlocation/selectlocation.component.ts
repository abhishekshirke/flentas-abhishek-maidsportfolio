import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import {NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { Maid } from '../user';
import { style } from '@angular/animations';



@Component({
  selector: 'app-selectlocation',
  templateUrl: './selectlocation.component.html',
  styleUrls: ['./selectlocation.component.css'],


})
export class SelectlocationComponent implements OnInit {

 
  //maid = new Maid();
  maids: Maid[];
  constructor(private _service : RegistrationService, private _router : Router) { }



  ngOnInit() {

    //this.showMaids();

  }
  
  selectedLocation: String = '';
  selectChange1(event: any) {

    this.selectedLocation = event.target.value;

  }

  selectedCategory: String = '';
  selectChange2(event: any) {

    this.selectedCategory = event.target.value;

  }

  showMaids() {
   console.log("show maids");

    //this._service.showMaidsFromRemote(this.maid).subscribe(
        this._service.showMaidsFromRemote(this.selectedLocation).subscribe((data) => {
          console.log(data);
          this.maids = data;

        });

    }

    //message = "asd"

    clickevent(s1: String) {

      this._service.setMessage(s1)
      //console.log(s1);

      this._router.navigate(['/showdetails'])
      console.log("clicked detais button");
    }
    
      //     data => { 
         
    //       this._router.navigate(['/show'])
    //       //this._router.navigate(['/loginsuccess'])
    //       //this._router.navigate(['/selectlocation'])
    //     } ,
        
    //     error => { 
          
    //       console.log("exception occured");
    //       //this.msg = "Bad Credentials, Please Enter Valid EmailId and Password";
        
    //   }
    // )

  }
