import { Component, OnInit } from '@angular/core';
import { Time} from '../user';
import {Mail} from '../user';
import { RegistrationService } from '../registration.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-bookslot',
  templateUrl: './bookslot.component.html',
  styleUrls: ['./bookslot.component.css'],

  
})
export class BookslotComponent implements OnInit {
 
  Time: Time[];
  message: string;
  mail = new Mail();
  getmail: String;

  constructor(private _service: RegistrationService,private _router : Router) { }

  ngOnInit(): void {
    
      this.message = this._service.getMessage()
      this.getmail = this._service.getUserMail()
      this._service.bookslotFromRemote(this.message).subscribe((data) => {
      console.log(data);
      this.Time = data;
      console.log(this.message)

    });

  }

  myFunction(t1: String, name: String) {

      console.log(t1,name);
      this._service.updateSlot(t1,name).subscribe((data) => {

        console.log("button clicked");
        //console.log(data);
        
        
    });

    this.getmail = this._service.getUserMail()
    this.mail.to_address = this.getmail;
    this.mail.body = " Your maid ".concat(name+" is booked at time "+t1); 
    //this.mail.subject = "Confirmation Mail";
    //this.mail.body = "Booking Confirmed";
    console.log(this.getmail);
  
    this._service.sendMail(this.mail).subscribe((data) => {

         console.log("mailsend");    

    });
    
    alert("Booking Confirmed");

  }
}
