import { Injectable } from '@angular/core';
import {Maid, User, Time, Mail} from './user';
import {Observable } from 'rxjs';
import {HttpClient, HttpHeaders, HttpHeaderResponse, HttpRequest} from '@angular/common/http';
import { __values } from 'tslib';
import { RouterLink } from '@angular/router';
//import { NOTFOUND } from 'dns';
const headeroption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  message: string
  userMail: String
  constructor(private _http : HttpClient) { } 

  setMessage(data) {
      
    this.message = data

  }
  setUserMail(data) {

    this.userMail = data
    //console.log(this.userMail)
  }
  getMessage() {
    
    return this.message

  }

  getUserMail() {

    return this.userMail

  }

  maids: Maid = {
      
      id: null,
      name: '',
      location: '',
      catagory: '',


  };

  public loginUserFromRemote(user :User):Observable<any> {

    return this._http.post<any>("http://localhost:9060/login",user)

  }
  public showMaidsFromRemote(location: String):Observable<Maid[]> {

    //return this._http.post<any>("http://localhost:9070/show",maid)
    return this._http.get<Maid[]>("http://localhost:9060/show?location="+location,headeroption);
  }

  public registerUserFromRemote(user :User):Observable<any> {

    return this._http.post<any>("http://localhost:9060/registeruser",user)

  }

  public bookslotFromRemote(name: String):Observable<Time[]> {

    //return this._http.post<any>("http://localhost:9070/show",maid)
    // return this._http.get<Time[]>("http://localhost:9070/bookslot"+name,headeroption);
    return this._http.get<Time[]>("http://localhost:9060/bookslot?name="+name);
    
  
  }

  public updateSlot(time: String, name: String):Observable<Time[]> {

    //return this._http.post<any>("http://localhost:9070/show",maid)
    // return this._http.get<Time[]>("http://localhost:9070/bookslot"+name,headeroption);
    //return this._http.put("http://localhost:9070/bookslot?time="+time);
    console.log(time,name);
    console.log("asdsad");
    const endpointURL = "http://localhost:9060/bookslot?time="+time+"&name="+name
    return this._http.put<Time[]>(endpointURL,headeroption);
  }

  public sendMail(mail :Mail):Observable<any> {
    console.log(mail.to_address);
    console.log(mail.body);
    console.log(mail.subject);
    return this._http.post<any>("http://localhost:9060/send",mail);

  }

}
