import { Component, OnInit } from '@angular/core';
import {RegistrationService} from '../registration.service'
//import { RegistrationService } from '../registration.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-showdetails',
  templateUrl: './showdetails.component.html',
  styleUrls: ['./showdetails.component.css']
})
export class ShowdetailsComponent implements OnInit {

  message: string

  constructor(private shared: RegistrationService,private _router : Router) { }

  ngOnInit(): void {

    this.message = this.shared.getMessage()
    //console.log(this.message)
  }

  clickevent() {

    this._router.navigate(['/bookslot'])

  }

}
